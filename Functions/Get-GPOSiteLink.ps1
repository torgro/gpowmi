function Get-GPOSiteLink
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipelineByPropertyName)]
    [Alias("SiteName")]
    [string]$Name = "*"
)

BEGIN
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    if(-not(Get-Module -Name ActiveDirectory))
    {
        Import-Module -Name ActiveDirectory -ErrorAction Stop -Verbose:$false
    }

    if(-not(Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }    

    Write-Verbose -Message "$f -  Creating sitesContainer"
    $forest = (get-addomain).forest
    $domainName = $env:UserDnsDomain
    $dc = "localhost"    
    $dcUsage = [Microsoft.GroupPolicy.DCUsage]::UseAnyDC
    
    $sitesContainer = New-Object -TypeName Microsoft.GroupPolicy.GPSitesContainer $forest,$domainName,$dc,$dcUsage 
}

PROCESS
{
    if($Name.Contains('*'))
    {
        Write-Verbose -Message "$f -  Finding all sites"
        $Name = (Get-ADReplicationSite).Name  | Where-Object {$_ -like $Name}      
    }
   
    Write-Verbose -Message "$f -  Processing site $Name"
    
    $siteObject = $sitesContainer.GetSite($Name)

    if($siteObject)
    {
        $siteObject.GpoLinks
    }
    else
    {
        Write-Verbose -Message "$f -  Could not find any GPOs at $name"
    }    
}

END
{
    Write-Verbose -Message "$f - END"
}
}