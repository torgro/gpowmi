﻿function Get-GPOWmiFilter
{
<#
.Synopsis
   Gets all Group Policy Filters or by name
.DESCRIPTION
   This cmdlet depends on the GroupPolicy module
.EXAMPLE
   Get-GPOWmiFilter

   Lists all (if any) WMIFilter configured in Group Policy Management
.EXAMPLE
   Get-GPOWmiFilter -Name HR

   Get a WMIFilter named HR
.EXAMPLE
   "HR" | Get-GPOWmiFilter

   Get a WMIFilter named HR using the pipeline
.EXAMPLE
   "HR","AllClients" | Get-GPOWmiFilter

   Gets WMIFilters named HR and Allclients using the pipeline
.INPUTS
   String (Name of WMIFilter)
.OUTPUTS
   Microsoft.GroupPolicy.WmiFilter
.NOTES
   Created by Tore.Groneng@firstpoint.no @ToreGroneng 2016
.ROLE
   Active Directory scripting
.FUNCTIONALITY
   Linking GPOs with WMIFilters
#>
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [string[]]$Name = "*"
)
BEGIN
{
    $f = $MyInvocation.InvocationName

    if(-not (Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }
    $server = "localhost"
    $domain = $env:userDnsDomain

    Write-Verbose -Message "$f - START"
    $GPdomain = New-Object Microsoft.GroupPolicy.GPDomain $domain,$server
    $SearchFilter = New-Object Microsoft.GroupPolicy.GPSearchCriteria
    Write-Verbose -Message "$f -  Searching for WmiFilters"
    $allWmiFilters = $GPdomain.SearchWmiFilters($SearchFilter)
    Write-Verbose -Message "$f -  Found $($allWmiFilters.Count) filters"
}

PROCESS
{
    foreach($FilterName in $Name)
    {
        Write-Verbose -Message "$f -  Looking for $FilterName"
        $allWmiFilters | Where-Object Name -like $FilterName
    }
}

END
{
    Write-Verbose -Message "$f - END"
}
}