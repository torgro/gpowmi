﻿function Set-GPOWmiFilter
{
<#
.Synopsis
   Links a Group Policy to a WMIFilter
.DESCRIPTION
   This cmdlet depends on the GroupPolicy module. Apply a WMI filter to any Group Policy configured in Active Directory
.EXAMPLE
   Set-GPOWmiFilter -WMIFilterName HR -GroupPolicyName MyGPO

   Links the WMIfilter named HR to the Group Policy MyGPO
.EXAMPLE
   Set-GPOWmiFilter -WMIFilterName HR -GroupPolicyGUID ed3119f9-4038-464e-abdc-9202bfd24917

   Links the WMIfilter named HR to the Group Policy with the specified GUID
.EXAMPLE
   Get-GPOWmiFilter -Name HR | Set-GPOWmiFilter -GroupPolicyName MyGPO

   Get a WMIFilter named HR and applies it using the pipeline to the Group Policy MyGPO
.OUTPUTS
   $null
.NOTES
   Created by Tore.Groneng@firstpoint.no @ToreGroneng 2016
.ROLE
   Active Directory scripting
.FUNCTIONALITY
   Linking GPOs with WMIFilters
#>
[cmdletbinding()]
Param(
    [Parameter(
        ValueFromPipeline,
        ParameterSetName='ByWMIFilterObject')]
    [Microsoft.GroupPolicy.WmiFilter]$WMIfilter
    ,
    [Parameter(ParameterSetName='ByWMIFilterName')]
    [Parameter(ParameterSetName='FilterByPolicyName')]
    [Parameter(ParameterSetName='FilterByPolicyGUID')]
    [string]$WMIFilterName
    ,
    
    [Parameter(ParameterSetName='FilterByPolicyName')]
    [Parameter(ParameterSetName='ByWMIFilterObject')]
    [string]$GroupPolicyName
    ,
    
    [Parameter(ParameterSetName='FilterByPolicyGUID')]
    [Parameter(ParameterSetName='ByWMIFilterObject')]
    [guid]$GroupPolicyGUID
)
BEGIN
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
    $server = "localhost"
    $domain = $env:userDnsDomain
    
    Write-Verbose -Message "$f -  Loading required module GroupPolicy"

    if(-not (Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }
    $GPdomain = New-Object Microsoft.GroupPolicy.GPDomain $domain,$Server
    $SearchFilter = New-Object Microsoft.GroupPolicy.GPSearchCriteria

    Write-Verbose -Message "$f -  Searching for WmiFilters"
    $allWmiFilters = $GPdomain.SearchWmiFilters($SearchFilter)
}

PROCESS
{    
    if($WMIFilterName)
    {
        Write-Verbose -Message "$f -  Finding WMI-filter with name $WMIFilterName"
        $WMIfilter = $allWmiFilters | Where-Object Name -eq $WMIFilterName
        if(-not $WMIfilter)
        {
            $msg = "Did not find a WMIfilter with name '$WMIFilterName'"
            Write-Verbose -Message "$f - ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    $GroupPolicyObject = $null

    if($GroupPolicyName)
    {
        Write-Verbose -Message "$f -  Finding Group Policy with name '$GroupPolicyName'"
        $GroupPolicyObject = Get-GPO -Name $GroupPolicyName -Server $server
        if(-not $GroupPolicyObject)
        {
            $msg = "Unable to find GPO with Name '$GroupPolicyName'"
            Write-Verbose -Message "$f -  ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    if($GroupPolicyGUID)
    {
        Write-Verbose -Message "$f -  Finding Group Policy with GUID '$GroupPolicyGUID'"
        $GroupPolicyObject = Get-GPO -Guid $GroupPolicyGUID -Server $server
        if(-not $GroupPolicyObject)
        {
            $msg = "Unable to find GPO with GUID '$GroupPolicyGUID'"
            Write-Verbose -Message "$f -  ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    Write-Verbose -Message "$f -  Applying filter with name '$($WMIfilter.Name)' to GPO '$($GroupPolicyObject.DisplayName)'"

    try
    {
        $GroupPolicyObject.WmiFilter = $WMIfilter
    }
    catch
    {
        $ex = $_.Exception
        Write-Verbose -Message "$f -  EXCEPTION - $($ex.Message)"
        throw $ex
    }
}

END
{
    Write-Verbose -Message "$f - END"
}
}