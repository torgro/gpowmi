﻿function Get-GPOSiteLink
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipelineByPropertyName)]
    [Alias("SiteName")]
    [string]$Name = "*"
)

BEGIN
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    if(-not(Get-Module -Name ActiveDirectory))
    {
        Import-Module -Name ActiveDirectory -ErrorAction Stop -Verbose:$false
    }

    if(-not(Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }    

    Write-Verbose -Message "$f -  Creating sitesContainer"
    $forest = (get-addomain).forest
    $domainName = $env:UserDnsDomain
    $dc = "localhost"    
    $dcUsage = [Microsoft.GroupPolicy.DCUsage]::UseAnyDC
    
    $sitesContainer = New-Object -TypeName Microsoft.GroupPolicy.GPSitesContainer $forest,$domainName,$dc,$dcUsage 
}

PROCESS
{
    if($Name.Contains('*'))
    {
        Write-Verbose -Message "$f -  Finding all sites"
        $Name = (Get-ADReplicationSite).Name  | Where-Object {$_ -like $Name}      
    }
   
    Write-Verbose -Message "$f -  Processing site $Name"
    
    $siteObject = $sitesContainer.GetSite($Name)

    if($siteObject)
    {
        $siteObject.GpoLinks
    }
    else
    {
        Write-Verbose -Message "$f -  Could not find any GPOs at $name"
    }    
}

END
{
    Write-Verbose -Message "$f - END"
}
}

function Get-GPOWmiFilter
{
<#
.Synopsis
   Gets all Group Policy Filters or by name
.DESCRIPTION
   This cmdlet depends on the GroupPolicy module
.EXAMPLE
   Get-GPOWmiFilter

   Lists all (if any) WMIFilter configured in Group Policy Management
.EXAMPLE
   Get-GPOWmiFilter -Name HR

   Get a WMIFilter named HR
.EXAMPLE
   "HR" | Get-GPOWmiFilter

   Get a WMIFilter named HR using the pipeline
.EXAMPLE
   "HR","AllClients" | Get-GPOWmiFilter

   Gets WMIFilters named HR and Allclients using the pipeline
.INPUTS
   String (Name of WMIFilter)
.OUTPUTS
   Microsoft.GroupPolicy.WmiFilter
.NOTES
   Created by Tore.Groneng@firstpoint.no @ToreGroneng 2016
.ROLE
   Active Directory scripting
.FUNCTIONALITY
   Linking GPOs with WMIFilters
#>
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [string[]]$Name = "*"
)
BEGIN
{
    $f = $MyInvocation.InvocationName

    if(-not (Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }
    $server = "localhost"
    $domain = $env:userDnsDomain

    Write-Verbose -Message "$f - START"
    $GPdomain = New-Object Microsoft.GroupPolicy.GPDomain $domain,$server
    $SearchFilter = New-Object Microsoft.GroupPolicy.GPSearchCriteria
    Write-Verbose -Message "$f -  Searching for WmiFilters"
    $allWmiFilters = $GPdomain.SearchWmiFilters($SearchFilter)
    Write-Verbose -Message "$f -  Found $($allWmiFilters.Count) filters"
}

PROCESS
{
    foreach($FilterName in $Name)
    {
        Write-Verbose -Message "$f -  Looking for $FilterName"
        $allWmiFilters | Where-Object Name -like $FilterName
    }
}

END
{
    Write-Verbose -Message "$f - END"
}
}

function Set-GPOWmiFilter
{
<#
.Synopsis
   Links a Group Policy to a WMIFilter
.DESCRIPTION
   This cmdlet depends on the GroupPolicy module. Apply a WMI filter to any Group Policy configured in Active Directory
.EXAMPLE
   Set-GPOWmiFilter -WMIFilterName HR -GroupPolicyName MyGPO

   Links the WMIfilter named HR to the Group Policy MyGPO
.EXAMPLE
   Set-GPOWmiFilter -WMIFilterName HR -GroupPolicyGUID ed3119f9-4038-464e-abdc-9202bfd24917

   Links the WMIfilter named HR to the Group Policy with the specified GUID
.EXAMPLE
   Get-GPOWmiFilter -Name HR | Set-GPOWmiFilter -GroupPolicyName MyGPO

   Get a WMIFilter named HR and applies it using the pipeline to the Group Policy MyGPO
.OUTPUTS
   $null
.NOTES
   Created by Tore.Groneng@firstpoint.no @ToreGroneng 2016
.ROLE
   Active Directory scripting
.FUNCTIONALITY
   Linking GPOs with WMIFilters
#>
[cmdletbinding()]
Param(
    [Parameter(
        ValueFromPipeline,
        ParameterSetName='ByWMIFilterObject')]
    [Microsoft.GroupPolicy.WmiFilter]$WMIfilter
    ,
    [Parameter(ParameterSetName='ByWMIFilterName')]
    [Parameter(ParameterSetName='FilterByPolicyName')]
    [Parameter(ParameterSetName='FilterByPolicyGUID')]
    [string]$WMIFilterName
    ,
    
    [Parameter(ParameterSetName='FilterByPolicyName')]
    [Parameter(ParameterSetName='ByWMIFilterObject')]
    [string]$GroupPolicyName
    ,
    
    [Parameter(ParameterSetName='FilterByPolicyGUID')]
    [Parameter(ParameterSetName='ByWMIFilterObject')]
    [guid]$GroupPolicyGUID
)
BEGIN
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
    $server = "localhost"
    $domain = $env:userDnsDomain
    
    Write-Verbose -Message "$f -  Loading required module GroupPolicy"

    if(-not (Get-Module -Name GroupPolicy))
    {
        Import-Module -Name GroupPolicy -ErrorAction Stop -Verbose:$false
    }
    $GPdomain = New-Object Microsoft.GroupPolicy.GPDomain $domain,$Server
    $SearchFilter = New-Object Microsoft.GroupPolicy.GPSearchCriteria

    Write-Verbose -Message "$f -  Searching for WmiFilters"
    $allWmiFilters = $GPdomain.SearchWmiFilters($SearchFilter)
}

PROCESS
{    
    if($WMIFilterName)
    {
        Write-Verbose -Message "$f -  Finding WMI-filter with name $WMIFilterName"
        $WMIfilter = $allWmiFilters | Where-Object Name -eq $WMIFilterName
        if(-not $WMIfilter)
        {
            $msg = "Did not find a WMIfilter with name '$WMIFilterName'"
            Write-Verbose -Message "$f - ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    $GroupPolicyObject = $null

    if($GroupPolicyName)
    {
        Write-Verbose -Message "$f -  Finding Group Policy with name '$GroupPolicyName'"
        $GroupPolicyObject = Get-GPO -Name $GroupPolicyName -Server $server
        if(-not $GroupPolicyObject)
        {
            $msg = "Unable to find GPO with Name '$GroupPolicyName'"
            Write-Verbose -Message "$f -  ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    if($GroupPolicyGUID)
    {
        Write-Verbose -Message "$f -  Finding Group Policy with GUID '$GroupPolicyGUID'"
        $GroupPolicyObject = Get-GPO -Guid $GroupPolicyGUID -Server $server
        if(-not $GroupPolicyObject)
        {
            $msg = "Unable to find GPO with GUID '$GroupPolicyGUID'"
            Write-Verbose -Message "$f -  ERROR - $msg"
            Write-Error -Message $msg -ErrorAction Stop
        }
    }

    Write-Verbose -Message "$f -  Applying filter with name '$($WMIfilter.Name)' to GPO '$($GroupPolicyObject.DisplayName)'"

    try
    {
        $GroupPolicyObject.WmiFilter = $WMIfilter
    }
    catch
    {
        $ex = $_.Exception
        Write-Verbose -Message "$f -  EXCEPTION - $($ex.Message)"
        throw $ex
    }
}

END
{
    Write-Verbose -Message "$f - END"
}
}


